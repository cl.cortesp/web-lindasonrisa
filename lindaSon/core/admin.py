from django.contrib import admin
from .models import Tipo, Especie, Sexo, Mascota, Color, TipoUsuario, Cuenta, Servicio

# Register your models here.

class MascotaAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'tipo', 'especie', 'sexo']
    search_fields = ['nombre']
    list_filter = ['especie', 'sexo', 'tipo']
    list_per_page = 10

admin.site.register(Tipo)
admin.site.register(Especie)
admin.site.register(Sexo)
admin.site.register(Color)
admin.site.register(Mascota, MascotaAdmin)
admin.site.register(TipoUsuario)
admin.site.register(Cuenta)
admin.site.register(Servicio)