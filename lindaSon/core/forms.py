from django import forms
from django.forms import ModelForm
from .models import Mascota
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class MascotaForm(ModelForm):

    nombre = forms.CharField(min_length=2, max_length=50)

    class Meta:
        model = Mascota
        fields  = ['nombre', 'tipo', 'especie', 'sexo', 'observaciones', 'imagen', 'nombreContacto', 'telefonoContacto']

class CustomUserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'username', 'password1', 'password2']
