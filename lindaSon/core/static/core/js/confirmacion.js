function confirmarEliminacion(id) {
    
    Swal.fire({
        title: '¿Estás seguro/a de eliminar el registro?',
        text: "¡No podrás deshacer esta acción!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, elimínalo!',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
            //redirigir al usuario a la ruta de eliminar
            window.location.href = "/eliminar_mascota/"+id+"/";
        }
      })
}